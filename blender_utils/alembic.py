import os
import bpy
import fnmatch


def get_cache_file(filepath):
    for cache_file in bpy.data.cache_files:
        if cache_file.filepath == filepath:
            return cache_file


def create_cache_file(filepath):
    """
    create an alembic file cache object

    :param filepath: alembic filepath
    :return file_cache object: the created cache file
    """

    if os.path.splitext(filepath)[-1] != ".abc":
        print("filepath is not an alembic")
        return

    # check if exists
    for cache_file in bpy.data.cache_files:
        if bpy.path.abspath(cache_file.filepath) == filepath:
            return cache_file

    bpy.ops.cachefile.open(filepath=filepath)
    basename = os.path.basename(filepath)
    cache_file = get_cache_file(filepath)

    # Init cache file for listing objects path
    obj = bpy.data.objects.get('abc')
    if not obj:
        obj = bpy.data.objects.new('abc', None)
    c = obj.constraints.new('TRANSFORM_CACHE')
    c.cache_file = cache_file
    bpy.context.view_layer.update()
    bpy.data.objects.remove(obj)

    return cache_file


def assign_alembic_to_objects(obj_list, alembic_filepath, is_match, type="modifier", **kwargs):
    """
    assign an alembic to a list of objects

    :param obj_list: a list of blender object on which apply the mesh sequence cache
    :param alembic_filepath: alembic filepath to import
    :param is_match: the function to see if alembic object path match with the current object
    :param read_data: which data will be used by alembic The value can be VERT, POLY, UV and COLOR
    :return bool: true if succeeded
    """

    from smks_studio.flow.common.dcc_scripts.blender.utils import clean_transforms

    cache_file = create_cache_file(alembic_filepath)
    if not cache_file:
        return False

    for obj in obj_list:

        try:
            obj_path = next(abc_obj_path for abc_obj_path in cache_file.object_paths if is_match(obj, abc_obj_path))
        except:
            continue

        if type == "modifier" and obj.type in ("CURVE", "MESH"):
            modifier = obj.modifiers.new(name="MeshSequenceCache", type='MESH_SEQUENCE_CACHE')
        elif type == "constraint" and obj.type in ("CURVE", "MESH", "EMPTY"):
            modifier = obj.constraints.new("TRANSFORM_CACHE")
        else:
            continue

        modifier.cache_file = cache_file
        modifier.object_path = obj_path.path

        for kw, value in kwargs.items():
            try:
                setattr(modifier, kw, value)
            except:
                setattr(cache_file, kw, value)

        # move modifier to top
        if type == "modifier":
            modifiers = obj.modifiers.values()
            modifiers.reverse()
            modifiers.remove(modifier)
            for i, mod in enumerate(modifiers):
                if mod.name == "MeshSequenceCache":
                    break
                bpy.ops.object.modifier_move_up({"object": obj}, modifier=modifier.name)


        clean_transforms(obj)

    return True


def load_new_filecache(objects, new_abc_filepath=None):
    """
    Replace all alembic by new version

    :param objects: all objects to update alembic
    :param new_abc_filepath: new alembic filepath
    :return: list of caches changed
    """

    changed = []

    all_abc = [m for obj in objects for m in obj.modifiers
               if m.type == "MESH_SEQUENCE_CACHE"]

    for abc in all_abc:

        # get all original alembic attributes
        current_read_data = abc.read_data
        current_object_path = abc.object_path

        new_cache_file = create_cache_file(new_abc_filepath)
        abc.cache_file = new_cache_file
        abc.object_path = current_object_path
        abc.read_data = current_read_data

        if new_cache_file.name not in changed:
            changed.append(new_cache_file.name)

    return changed


def import_alembic(alembic_filepath, **kwargs):
    bpy.ops.wm.alembic_import(filepath=alembic_filepath, **kwargs)


def have_alembic(objects):
    try:
        iter(objects)
    except TypeError:
        objects = [objects]

    for obj in objects:
        for stack in obj.modifiers.values() + obj.constraints.values():
            if stack.type == 'MESH_SEQUENCE_CACHE' or stack.type == 'TRANSFORM_CACHE':
                return True

    return False


def remove_abc(objects):

    if not isinstance(objects, list):
        objects = [objects]

    for obj in objects:
        for stack in obj.modifiers.values():
            if stack.type == 'MESH_SEQUENCE_CACHE':
                obj.modifiers.remove(stack)

        for stack in obj.constraints.values():
            if stack.type == 'TRANSFORM_CACHE':
                obj.constraints.remove(stack)
