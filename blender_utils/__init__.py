import os
import logging
import re
import fnmatch

try:
    import bpy
    from bpy_types import Collection
except ImportError:
    print("Cannot import bpy ! You should use this module in Blender")
    bpy = None

IMG_EXTS = ['.tiff', '.tif', '.exr', '.jpeg', '.jpg', '.png']
blender_id_suffix_regex = re.compile(r"\.(\d{3})$")


# COMMONS

def get_range():
    return bpy.context.scene.frame_start, bpy.context.scene.frame_end = end


def get_camera(names):
    import fnmatch

    if not isinstance(names, list):
        names = [names]

    for name in names:
        for obj in bpy.data.objects:
            if obj.type == "CAMERA" and fnmatch.fnmatch(obj.name, name):
                return obj


# OBJECT

def set_object_as_current(obj):
    """
    set current object as active and force its selection
    """
    if bpy.context.view_layer.objects.active:
        bpy.context.view_layer.objects.active.select_set(state=False)
    if obj.name in bpy.context.view_layer.objects:
        activeObject = bpy.context.view_layer.objects.active = obj
        activeObject.select_set(state=True)


def select(objects, replace=True):
    object = None
    if replace:
        unselect_all()
    for object in objects:
        object.hide_select = False
        try:
            object.select_set(state=True)
            if object.instance_type == "COLLECTION" and object.instance_collection:
                object.instance_collection.hide_select = False
                # select(object.instance_collection.all_objects, False)
                object.instance_collection.select_set(state=True)
        except Exception as e:
            print(e)
    if object:
        set_object_as_current(object)


def unselect_all():
    bpy.context.view_layer.objects.active = None
    for object in bpy.data.objects:
        object.select_set(state=False)


def selected_objects():
    return [object for object in bpy.data.objects if object.select_get()]


def reset_transforms():
    """
    reset selected object translate, rotate and scale transforms
    """
    bpy.ops.object.location_clear(clear_delta=False)
    bpy.ops.object.rotation_clear(clear_delta=False)
    bpy.ops.object.scale_clear(clear_delta=False)


# COLLECTIONS

def create_collection(collectionName, parent=None, objects=None):
    """
    Create a collection if doesn't exists

    Args:
        collectionName (str): the collection name to create.
        parent (blender object): a collection to link inside
    Returns:
        bpy.types.Collection: the collection querried
    """

    try:
        collection = bpy.data.collections[collectionName]
    except KeyError:
        collection = bpy.data.collections.new(collectionName)
        if parent:
            parent.children.link(collection)
        else:
            bpy.context.scene.collection.children.link(collection)

    if objects:
        for object in objects:
            collection.objects.link(object)

    return collection


def find_collection(collection_pattern):
    import fnmatch

    for collection in bpy.data.collections:
        if fnmatch.fnmatch(collection.name, collection_pattern):
            return collection
    return None


def switch_to_collection(objects, from_col, to_col):
    """
    Qwitch objects from one collection to another

    Args:
        objects (list): all bpy.types.Objects to move
        from_col (bpy.types.Collection): collection source
        to_col (bpy.types.Collection): collection destination
    """

    for obj in objects:
        to_col.objects.link(obj)
        from_col.objects.unlink(obj)


def set_active_collection(col_name):
    """Set active collection

    Args:
        col_name (str): the collection name to set active.
    Returns:
        bpy.types.Collection: the collection or None if failed.
    """

    layer_col = find_layer_collection(col_name)
    if layer_col:
        bpy.context.view_layer.active_layer_collection = layer_col
    return layer_col


def get_parent_collection(collection, parents):
  for parent_collection in bpy.data.collections:
    if collection.name in parent_collection.children.keys():
      parents.append(parent_collection)
      get_parent_collection(parent_collection, parents)
      return


def get_collections_hierarchy(obj):
    """Get a ordered list of all collections hierarchy of an object

    Args:
        obj (bpy.types.Object): object to evaluate.
    Returns:
        list: all bpy.types.Collection who are object parent
    """

    root_collection = obj.users_collection[0]
    parents = []
    parents.append(root_collection)
    get_parent_collection(root_collection, parents)
    parents.reverse()
    return parents


def remove_all_in_collection(collection):
    """Remove objects and collections in collection

    Args:
        collection (bpy.types.Collection): collection to evaluate.
    """

    for obj in collection.all_objects:
        bpy.data.objects.remove(obj)

    for child in collection.children:
        remove_all_in_collection(child)

    bpy.data.collections.remove(collection)


def find_layer_collection(col_name, layer_col=None, view_layer=None):
    """find recursively layer collection by collection

    Args:
        col_name (str): collection name
        layer_col (bpy.types.layerCollection, optional): the layer collection to start
            research. Defaults to None.
        view_layer (bpy.types.ViewLayer, optional): view layer to find layer collection
    Returns:
        bpy.types.layerCollection: the layer collection found
    """

    if not view_layer:
        view_layer = bpy.context.scene.view_layers[0]

    if not layer_col:
        layer_col = find_layer_collection(col_name, layer_col=view_layer.layer_collection, view_layer=view_layer)
        if layer_col:
            return layer_col
        return None

    if layer_col.name == col_name:
        return layer_col

    for layer in layer_col.children:
        col = find_layer_collection(col_name, layer_col=layer, view_layer=view_layer)
        if col:
            return col
    return None


# IMPORT EXPORT

def import_external_data(filepath, collection_pattern, id=1, link=True, instanciate=True, destination=None):
    """
    Link an external collection

    Args:
        filepath (str): Blender filepath to import
        collection_pattern (str): the collection to link (Default is "*")
        id (int): the id of the collection, usefull to numerotate (Default is 1)
        instanciate (bool): to import as collection instance (Default is True)
        instanciate (bool): to link collection (Default is True)
        destination_scene (blender scene): the scene to link the collection (Default is None)

    Returns:
        None: None
    """

    if not destination:
        destination = bpy.context.scene.collection

    # if lib already exists for instance collection, use the already linked collection
    current_lib = get_library(filepath)
    
    if current_lib:
        print("already in blender")

        external_cols = [user.copy() for user in current_lib.users_id if isinstance(user, bpy.types.Collection) if
                            fnmatch.fnmatch(user.name, collection_pattern)]
    
    else:
        with bpy.data.libraries.load(filepath=filepath, link=link) as (data_src, data_dst):
            data_dst.collections = [col for col in data_src.collections if fnmatch.fnmatch(col, collection_pattern)]

        current_lib = get_library(filepath)
        external_cols = data_dst.collections
            
            
    for col in external_cols:
        
        collection_name = col.name.rsplit('.', 1)[0] + ".%03d" % id
        
        if instanciate:
            instance = bpy.data.objects.new(collection_name, None)
            instance.instance_type = "COLLECTION"
            instance.instance_collection = col
            
            destination.objects.link(instance)
        
        else:
            destination.children.link(col)
            col.name = collection_name
            
            for child in col.children:
                child.make_local()
                child.name = child.name.rsplit('.', 1)[0] + ".%03d" % int_id

            # make local object and drived materials + rename by id
            if not link:
                for obj in col.all_objects:
                    obj.select_set(True)
                bpy.ops.object.make_local(type='SELECT_OBDATA_MATERIAL')
                utils.unselect_all()
                
                make_images_local(current_lib)

            for obj in col.all_objects:
                obj.name = re.sub(r"\.\d+$", ".%03d" % id, obj.name)
                
    # remove_corresponding lib if not used
    if not current_lib.users_id:
        bpy.data.libraries.remove(current_lib)


def import_alembic(alembic_path, namespace=""):
    if namespace:
        collection = create_collection(namespace)
        set_active_collection(collection.name)
        bpy.ops.wm.alembic_import(filepath=alembic_path, relative_path=False)
    else:
        collection = bpy.context.collection
    bpy.ops.wm.alembic_import(filepath=alembic_path, relative_path=False)
    return collection.all_objects


def save_scene(filepath=None, messager=None):
    """
    save scene as new version.
    save as .v000 if does not exists
    """
    if messager:
        messager("Save %s" % (filepath or bpy.data.libraries[0].filepath))
    bpy.context.preferences.filepaths.use_relative_paths = False
    bpy.ops.file.make_paths_absolute()

    if filepath:
        bpy.ops.wm.save_as_mainfile(filepath=filepath, relative_remap=False)
    else:
        bpy.ops.wm.save_mainfile(relative_remap=False)


def export_alembic(destination_file, objects_to_export, start=-1, end=-1, export_hierarchy=True, scale=1.0):
    if export_hierarchy:
        for o in objects_to_export:
            if o.parent and o.parent not in objects_to_export:
                objects_to_export.append(o.parent)

    objects_to_export = set(objects_to_export)
    conform_names(objects_to_export)

    if start < 0:
        start = bpy.context.scene.frame_start
    if end < 0:
        end = bpy.context.scene.frame_end

    select(objects_to_export)

    bpy.ops.wm.alembic_export(filepath=destination_file, global_scale=scale, start=start,
                              end=end, selected=True)


def get_library(filepath):
    
    all_lib = list(filter(lambda lib: lib.filepath == filepath, bpy.data.libraries))
    if all_lib:
        return all_lib[0]
    else:
        return None


def make_images_local(lib):
    for user in lib.users_id:
        if not isinstance(user, bpy.types.Image):
            continue

        user.user_remap(user.copy())


def reset_normals(objects):
    for object in objects:
        if not object.data:
            continue
        normals = []
        for v in object.data.vertices:
            normals.append(v.normal)
        object.data.normals_split_custom_set_from_vertices(normals)
