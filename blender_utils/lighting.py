#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""List of general lighting functions for blender"""

__author__ = "Florentin LUCE"
__email__ = "florentinluce@orange.fr"
__version__ = "1.0.1"


import bpy
import json

from .shading import get_nodes, find_material_output_node
from .. import blender_utils as utils


def ensure_cycles_loaded():
    """Ensure render engine is Cycles"""

    if bpy.context.scene.render.engine != 'CYCLES':
        print("Render engine must be Cycles")
        bpy.context.scene.render.engine = "CYCLES"


def create_view_layer(name, copy=True):
    """
    create view layer

    Args:
        name (str): the name of the view layer to create
        copy (bool, optional): if view layer must be copied or not
    """

    view_layers = bpy.context.scene.view_layers

    if view_layers.get(name):
        return view_layers.get(name)
    else:
        return view_layers.new(name)


def set_layer_collection_visibility(layer_collection, type, view_layer=None):
    """
    set visibility on a layer collection and its children

    Args:
        layer collection (bpy.types.LayerCollection): layer collection to evaluate
        type (str): view_layer where you create the AOV
        view_layer (bpy.types.ViewLayer, optional) the view layer affected by the script
    """

    if not view_layer:
        view_layer = bpy.context.scene.view_layers[0]
    
    if isinstance(layer_collection, str):
        layer_collection = utils.find_layer_collection(layer_collection, view_layer=view_layer)

    setattr(layer_collection, type, True)
    for child in layer_collection.children:
        set_layer_collection_visibility(child, type, view_layer=view_layer)


def create_aov(aov_name, view_layer=None):
    """
    create AOV

    Args:
        aov_name (str): the name of the custom AOV to create
        objects (bpy.types.ViewLayer): view_layer where you create the AOV
    """

    ensure_cycles_loaded()

    if not view_layer:
        for view_layer in bpy.context.scene.view_layers:
            create_aov(aov_name, view_layer=view_layer)

    aov = view_layer.cycles.aovs.add()
    aov.name = aov_name

    return aov


def assign_aov(aov_name, objects=None, aov_library_file=None):
    """
    assign a custom aov on objects materials

    Args:
        aov_name (str): the name of the custom AOV to create and get from a library file
        objects (list): list of blender objects that need the AOV shader
        aov_library_file (string): the .blend where the AOV shader's node_tree is stored
    """

    ensure_cycles_loaded()

    for view_layer in bpy.context.scene.view_layers:
        if view_layer.cycles.aovs.get(aov_name):
            continue
        create_aov(aov_name, view_layer=view_layer)

    if aov_name not in [nd_grp.name for nd_grp in bpy.data.node_groups]:
        with bpy.data.libraries.load(filepath=aov_library_file, link=False) as (data_src, data_dst):
            data_dst.node_groups = [nd_grp for nd_grp in data_src.node_groups if aov_name in nd_grp]

    if not bpy.data.node_groups.get(aov_name):
        raise ValueError("the custom aov %s does not exists in %s" % (aov_name, aov_library_file))

    if objects:
        materials_to_aov = [slot.material for obj in objects for slot in obj.material_slots]
    else:
        materials_to_aov = bpy.data.materials

    for mat in materials_to_aov:
        node_tree = mat.node_tree

        if not node_tree:
            continue

        posx = 0
        posy = 0
        output_node, links = find_material_output_node(mat)
        if output_node:
            posx = output_node.location[0]
            posy = output_node.location[1]

        # create the node group
        aov_node_group = node_tree.nodes.new("ShaderNodeGroup")
        aov_node_group.node_tree = bpy.data.node_groups.get(aov_name)
        aov_node_group.location = (posx - 200, posy + 200)

        # create the output aov node
        aov_output = node_tree.nodes.new("ShaderNodeOutputAOV")
        aov_output.name = aov_name
        aov_output.location = (posx, posy + 200)

        node_tree.links.new(aov_node_group.outputs[0], aov_output.inputs[0])


def apply_render_settings(json_path):
    """
   use a json file to set render setting, aov, etc.

   Args:
       json_path (str): filepath of the json containing all attributes to set
   """

    with open(json_path) as settings_file:
        data = json.load(settings_file)

        passes_settings = data["passes"]
        render_settings = data["settings"]

    for attr in render_settings.keys():
        root_attr = getattr(bpy.context.scene, attr)

        if attr == "view_layers":
            for view_layer_name, param in render_settings[attr].items():
                view_layer = create_view_layer(view_layer_name)

                for sub_attr, value in param.items():
                    if sub_attr == "passes":
                        for passe_name, is_used in value.items():
                            if passe_name == "aov":
                                for aov_name in is_used:
                                    create_aov(aov_name, view_layer=view_layer)
                            else:
                                set_attr(view_layer, {passes_settings[passe_name]["attr"]: is_used})
                    else:
                        set_attr(view_layer, {sub_attr: value})
        else:
            set_attr(root_attr, data["settings"][attr])


def create_output_tree(data_passes):

    scene = bpy.context.scene
    scene.use_nodes = True

    node_tree = scene.node_tree

    node_layers = get_nodes(node_tree, attr="type", value="R_LAYERS")
    if not node_layers:
        node_layers = node_tree.nodes.new("CompositorNodeRLayers")

    node_outputs = get_nodes(node_tree, attr="type", value="OUTPUT_FILE")
    if not node_outputs:
        node_outputs = node_tree.nodes.new("CompositorNodeOutputFile")

    for passe, settings in data_passes.items():

        if "Crypto" in passe:
            continue

        input = node_outputs.file_slots.get(settings["output_name"])
        if not input:
            input = node_outputs.file_slots.new(settings["output_name"])

        node_tree.links.new(node_layers.outputs[passe], input)


def set_attr(root, data):
    """
    set an attribute from a dict with recursivity

    Args:
        root (object instance): the blender instance to start
        data (dict): a dict as {'attr': 'value'}
    """

    for attr, value in data.items():

        if isinstance(value, dict):
            set_attr(attr, value)

        elif "." in attr:
            attr_splitted = attr.split(".")
            for n in range(len(attr_splitted) - 1):
                sub_attr = getattr(root, attr_splitted[n])

            attr = attr_splitted[-1]
            setattr(sub_attr, attr, value)

        else:
            setattr(root, attr, value)
