#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""List of general shading functions for blender"""

__author__ = "Florentin LUCE"
__email__ = "florentinluce@orange.fr"
__version__ = "1.0.1"


import bpy


def find_material_output_node(material, name=None):
    """
    Find the material Output and shaders connected to its "Surface" input

    Args:
        material (bpy.types.Material): material to find output node in tree
        name (str, optional): name of the material output
    """

    material.use_nodes = True
    node_tree = material.node_tree

    if not node_tree:
        return None, []

    if name:
        name = str(name).lower()

    for node in node_tree.nodes:
        if 'OUTPUT' in node.bl_static_type:
            if name:
                if name not in (node.label.lower(), node.name.lower()):
                    continue
            else:
                if not node.is_active_output:
                    continue
            input = node.inputs["Surface"]
            return node, [link.from_node for link in input.links]

    raise RuntimeError("Cannot find material shader node")


def set_active_output_node(material, name):
    """
    Set specified name as active output node

    Args:
        material (bpy.types.Material): material to evalutae
        name (str): name of the material output node
    """

    material.use_nodes = True
    node_tree = material.node_tree

    if not node_tree:
        return

    name = str(name).lower()

    for node in node_tree.nodes:
        if 'OUTPUT' in node.bl_static_type:
            if name in (node.label.lower(), node.name.lower()):
                node.is_active_output = True
                return
    raise RuntimeError("Cannot find material shader node with name %r" % name)


def create_texture_node_from_image(material, image):
    """
    create a texture node from blender image

    Args:
        material (bpy.types.Material): material to find output node in tree
        image (Blender image): image to use in a texture node
    """

    if isinstance(image, str):
        image = bpy.data.images.load(image)
    tex_image_node = material.node_tree.nodes.new("ShaderNodeTexImage")
    tex_image_node.image = image
    return tex_image_node


def connect_material_nodes(material, src_node, output_name, dst_node, input_name):
    """
    Connect material nodes

    Args:
        material (bpy.types.Material): material to find output node in tree
        src_node (bpy.types.Node): source node
        dst_node (bpy.types.Node): destination node
        output_name (str): name of the output
        input_name (str): name of the input 
    """

    return material.node_tree.links.new(dst_node.inputs[input_name], src_node.outputs[output_name])


def get_nodes(node_tree, attr=None, value=None):
    """
    get all nodes you want

    Args:
        node_tree (bpy.types.NodeTree): the node tree where you want to search for nodes
        attr (str): the attribute used to search
        value (str): the value the attribute have to match

    Returns:
        list or bpy.types.Node: all nodes found
    """

    if not hasattr(node_tree.nodes[0], attr):
        raise ValueError("this attribute %s does not exist for node object" % attr)

    if attr and value:
        all_nodes = [node for node in node_tree.nodes if getattr(node, attr) == value]
    elif attr and not value:
        all_nodes = [node for node in node_tree.nodes if hasattr(node, attr)]
    else:
        all_nodes = node_tree.nodes.values()

    if len(all_nodes) == 1:
        return all_nodes[0]

    return all_nodes


def switch_material_slot(obj, target="OBJECT", make_local=False):
    """
    link materials object on DATA or OBJECT slot

    Args:
        obj (bpy.types.Object): blender object
        target (str): "OBJECT" or "DATA"
        make_local (bool): if the material must be made local
    """

    for slot in obj.material_slots:
        mat = slot.material
        slot.link = target
        slot.material = mat

        if make_local:
            mat.make_local()
