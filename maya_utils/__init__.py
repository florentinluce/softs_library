#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""List of general utils functions for maya"""

__author__ = "Florentin LUCE"
__email__ = "florentinluce@orange.fr"
__version__ = "1.0.1"


import os

try:
    import maya.cmds as cmds
    import maya.mel as mel
except ImportError as e:
    print("Cannot import cmds ! You should use this module in Maya")
    cmds = None
    mel = None


FRAMERATE = {
    15: 'game',
    24: 'film',
    25: 'pal',
    30: 'ntsc',
    48: 'show',
    50: 'palf',
    60: 'ntscf',
}

AXES_TRANSFORM_ATTRS = ('tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz')
TRANSFORM_ATTRS = ('translate', 'rotate', 'scale')


def ls(filter='', **kwargs):
    if not filter and kwargs:
        return cmds.ls(**kwargs)
    return cmds.ls(filter, **kwargs)


def get_selection():
    return cmds.ls(selection=True)


# MAYA SETTINGS

def get_range():
    return cmds.playbackOptions(q=True, min=True), cmds.playbackOptions(q=True, max=True)


def set_range(first, last):
    print('Updating Playback and Animation Range to %s->%s' % (first, last))
    cmds.playbackOptions(min=first, ast=first, max=last, aet=last)


def set_framerate(rate):
    units = FRAMERATE.get((rate)
    cmds.currentUnit(time=units)
    cmds.optionVar(stringValue=('workingUnitTimeDefault', units))


def get_framerate():
    time_unit = cmds.currentUnit(q=True, time=True)
    return FRAMERATE.index(time_unit)


def apply_maya_settings(settings):
    """

    :param settings: dict with
     * padding=4
     * height=1080
     * frame_rate=25
     * start=-1 # do not set range start
     * end=-1 # do not set range end
     * width=1920
     * maya_project_path=<str>
     * user_away_path=<str>
    :param print:
    :return:
    """

    frame_rate = settings.get('frame_rate') or 25

    set_framerate(frame_rate)

    w = settings.get('width', 10)
    h = settings.get('height', 10)
    if w != -1:
        cmds.setAttr("defaultResolution.width", w)
    if h != -1:
        cmds.setAttr("defaultResolution.height", h)

    padding = settings.get('padding', 4)
    cmds.setAttr("defaultRenderGlobals.extensionPadding", padding)

    calculated_dar = w / (h * 1.0)
    cmds.setAttr("defaultResolution.deviceAspectRatio", calculated_dar)

    start = settings.get('start', -1)
    if start >= 0:
        cmds.playbackOptions(min=start)
    end = settings.get('end', -1)
    if end >= 0:
        cmds.playbackOptions(max=end)

    proj_path = settings.get('maya_project_path')
    if proj_path is None:
        print('   Warning: Maya Project Path not found in settings.')
    else:
        try:
            os.makedirs(proj_path)
        except OSError:
            pass
        mel.eval('setProject("%s")' % proj_path)

    color_config = settings.get("ocio_config_file")
    if color_config and os.path.exists(color_config):
        rendering_space = settings.get("rendering_color_space")
        color_config = color_config.replace("\\", "/")
        print('Applying color space %r' % rendering_space)
        cmds.colorManagementPrefs(e=True, configFilePath=color_config)
        cmds.colorManagementPrefs(e=True, cmEnabled=True)
        cmds.colorManagementPrefs(e=True, cmConfigFileEnabled=True)
        cmds.colorManagementPrefs(e=True, ocioRulesEnabled=True)

        monitor_space = settings.get("monitor_color_space")
        if not monitor_space.endswith("(ACES)"):
            monitor_space = monitor_space + " (ACES)"
        cmds.colorManagementPrefs(e=True, viewTransformName=monitor_space)
        cmds.colorManagementPrefs(e=True, renderingSpaceName=rendering_space)
        cmds.colorManagementFileRules('Default', edit=True, colorSpace='Utility - sRGB - Texture')

    user_away_path = settings.get('user_away_path')
    if user_away_path is None:
        print('   Warning: User Away Path not found in settings.')
    else:
        try:
            os.makedirs(proj_path)
        except OSError:
            pass


def set_resolution(w=1920, h=1080):
    cmds.setAttr('defaultResolution.width', w)
    cmds.setAttr('defaultResolution.height', h)


def get_resolution():
    w = cmds.getAttr('defaultResolution.width')
    h = cmds.getAttr('defaultResolution.height')
    return int(w), int(h)


# FILE MANAGER

def open_new_scene(scene_filename, maya_settings):

    try:
        os.makedirs(os.path.dirname(scene_filename))
    except OSError:
        pass

    cmds.file(newFile=True, force=True)
    if scene_filename:
        cmds.file(rename=scene_filename)

    apply_maya_settings(maya_settings)


def open_scene(scene_filename, maya_settings=None):
    """
    Opens the scene and apply settings (unless maya_settings is None)
    """

    print('Opening Scene %r' % scene_filename)
    try:
        if os.path.realpath(cmds.file(sn=True, q=True)) != os.path.realpath(scene_filename):
            cmds.file(scene_filename, open=True, force=True)
            print('Current Scene: %r' % cmds.file(sn=True, q=True))
        else:
            print('Current Scene is already %r' % scene_filename)
    except Exception:
        import traceback
        print(traceback.format_exc())
    cmds.file(rename=scene_filename)
    if maya_settings is not None:
        apply_maya_settings(maya_settings, print)


def rename_current_scene(scene_path):
    print('Rename scene into %r' % scene_path)
    try:
        os.makedirs(os.path.dirname(scene_path))
    except:
        pass
    cmds.file(rename=scene_path)


def save_scene(scene_filename=''):
    try:
        os.makedirs(os.path.dirname(scene_filename))
    except OSError:
        pass
    if scene_filename:
        ext = scene_filename.rsplit('.', 1)[-1]
        if ext not in ('ma', 'mb'):
            scene_filename = scene_filename + ".ma"
        if os.path.abspath(scene_filename) != os.path.abspath(cmds.file(q=True, sn=True)):
            cmds.file(rename=scene_filename)

    file_path = scene_filename or cmds.file(q=True, sn=True)

    attempt = 0
    while attempt < 3:
        try:
            if file_path.endswith('.ma'):
                print('Saving ascii scene %r' % file_path)
                cmds.file(save=True, force=True, type="mayaAscii")
            else:
                print('Saving binary scene %r' % file_path)
                cmds.file(save=True, force=True, type="mayaBinary")
        except Exception as e:
            print("Error when saving %r" % e)
            for node in cmds.ls(type="unknown"):
                if cmds.objExists(node):
                    cmds.lockNode(node, l=False)
                    cmds.delete(node)
        else:
            break
        attempt = attempt + 1
    if attempt >= 3:
        cmds.file(save=True, force=True)


def save_copy(file_path):

    try:
        os.makedirs(os.path.dirname(file_path))
    except OSError:
        pass

    ext = file_path.rsplit('.', 1)[-1]
    if ext not in ('ma', 'mb'):
        file_path = file_path + ".ma"

    attempt = 0
    original_path = cmds.file(q=True, sn=True) or "Untitled.ma"
    cmds.file(rename=file_path)
    while attempt < 3:
        try:
            if file_path.endswith('.ma'):
                print('Export ascii scene %r' % file_path)
                cmds.file(force=True, save=True, type="mayaAscii")
            else:
                print('Export binary scene %r' % file_path)
                cmds.file(force=True, save=True, type="mayaBinary")
        except Exception as e:
            print("Error when exporting %r" % e)
            for node in cmds.ls(type="unknown"):
                if cmds.objExists(node):
                    cmds.lockNode(node, l=False)
                    cmds.delete(node)
        else:
            break
        attempt = attempt + 1
    if attempt >= 3:
        cmds.file(force=True, save=True)
    cmds.file(rename=original_path)


def export_scene(file_path, objects=None):

    try:
        os.makedirs(os.path.dirname(file_path))
    except OSError:
        pass

    ext = file_path.rsplit('.', 1)[-1]
    if ext not in ('ma', 'mb'):
        file_path = file_path + ".ma"

    if not objects:
        objects = cmds.ls(transforms=True)

    objects += get_relatives(objects)

    attempt = 0
    while attempt < 3:
        cmds.select(cmds.ls(objects))
        try:
            if file_path.endswith('.ma'):
                print('Export ascii scene %r' % file_path)
                cmds.file(file_path, force=True, exportSelected=True, type="mayaAscii")
            else:
                print('Export binary scene %r' % file_path)
                cmds.file(file_path, force=True, exportSelected=True, type="mayaBinary")
        except Exception as e:
            print("Error when exporting %r" % e)
            for node in cmds.ls(type="unknown"):
                if cmds.objExists(node):
                    cmds.lockNode(node, l=False)
                    cmds.delete(node)
        else:
            break
        attempt = attempt + 1
    if attempt >= 3:
        cmds.file(file_path, force=True, exportSelected=True)


# TRANSFORMS

def remove_transforms(objects):
    for object in objects:
        for attr in AXES_TRANSFORM_ATTRS:
            if not cmds.attributeQuery(attr, node=object, exists=True):
                continue
            cmds.setAttr("%s.%s" % (object, attr), lock=False)
            try:
                if attr.startswith('s'):
                    cmds.setAttr("%s.%s" % (object, attr), 1)
                else:
                    cmds.setAttr("%s.%s" % (object, attr), 0)
            except RuntimeError:
                pass


def unlock_transforms(object):
    for attr in AXES_TRANSFORM_ATTRS:
        if not cmds.attributeQuery(attr, node=object, exists=True):
            continue
        cmds.setAttr("%s.%s" % (object, attr), lock=False)
    for attr in TRANSFORM_ATTRS:
        if not cmds.attributeQuery(attr, node=object, exists=True):
            continue
        cmds.setAttr("%s.%s" % (object, attr), lock=False)
    try:
        cmds.setAttr("%s.inheritsTransform" % object, lock=False)
        cmds.setAttr("%s.inheritsTransform" % object, True)
    except Exception:
        pass


def clean_transforms(objects):
    for object in objects:
        unlock_transforms(object)
        cmds.makeIdentity(
            object, apply=True,
            translate=True, rotate=True, scale=True,
            normal=True, preserveNormals=True
        )


def delete_history(objects):
    for obj in objects:
        cmds.delete(obj, constructionHistory=True)


def get_world_transformations(object):
    translates = cmds.xform(object, query=True, worldSpace=True, t=True)
    rotates = cmds.xform(object, query=True, worldSpace=True, ro=True)
    scales = cmds.xform(object, query=True, worldSpace=True, s=True)
    return translates, rotates, scales


def get_transformations(object):
    translates = cmds.xform(object, query=True, t=True)
    rotates = cmds.xform(object, query=True, ro=True)
    scales = cmds.xform(object, query=True, s=True)
    return translates, rotates, scales


def set_transformations(object, translates, rotates, scales):
    cmds.xform(object, t=translates)
    cmds.xform(object, ro=rotates)
    cmds.xform(object, s=scales)


# HIERARCHY AND OBJECTS PATH

def create_group_hierarchy(path):
    """Create a succession of empty transforms if they do not exist"""

    tmpPath = ''
    for i in path.strip('|').split('|'):
        if cmds.objExists(tmpPath + '|' + i):
            pass
        else:
            group = cmds.group(empty=True, name=i)
            if tmpPath:
                parented = cmds.parent(group, tmpPath)
                cmds.rename(parented, i)
        tmpPath = tmpPath + '|' + i


def get_root(all_objects):
    """Get all the first elements from maya long name objects

    Args:
        all_objects (list): all objects from a hierarchy
    Returns:
        list: all first element found
    """

    return [obj for obj in all_objects if len(obj.split("|")) <= 2]


def long_name_of(node):
    """Return the full path of a node"""
    return cmds.ls(node, long=True)[0]


def short_name_Of(node):
    """Return the short name of a node"""
    return node.split('|')[-1]


# SET OPERATIONS

def add_objects_to_set(objects, set_name):
    cmds.select(objects)
    add_selection_to_set(set_name)


def remove_objects_from_set(objects, set_name):
    cmds.select(objects)
    remove_selection_from_set(set_name)


def add_selection_to_set(name):
    selected = cmds.ls(sl=True) or []
    try:
        cmds.sets(selected, addElement=name)
    except:
        print("set %s does not exist. It will be created")
        cmds.sets(name=name)


def remove_selection_from_set(name):
    selected = cmds.ls(sl=True) or []
    try:
        cmds.sets(selected, remove=name)
    except:
        print("set %s does not exist. It will be created")
        cmds.sets(name=name)


def list_set_content(set_name):
    sets = cmds.ls(set_name, sets=True, l=True)
    if not sets:
        return []
    try:
        content = cmds.ls(cmds.sets(sets, q=True), l=True)
    except ValueError:
        return []
    if content:
        return content + (cmds.listRelatives(content, allDescendents=True, fullPath=True) or [])
    return []


# IMPORT AND REFERENCES

def add_reference(scene_path, parent=None, namespace='', loaded=True):
    print('Adding Reference to (%s:)%s' % (namespace, scene_path))

    file_type = "mayaAscii"
    if scene_path.endswith('.abc'):
        ensure_alembic_loaded(print)
        file_type = "Alembic"
    elif scene_path.endswith('.mb'):
        file_type = "mayaBinary"

    new_nodes = cmds.file(
        scene_path,
        reference=True,
        type=file_type,
        namespace=namespace,
        returnNewNodes=True,
    )
    if parent and cmds.objExists(parent):
        parented = []
        for root in reference_roots(namespace):
            new_root = cmds.parent(root, parent)
            parented += new_root + cmds.listRelatives(new_root, allDescendents=True)
        return parented + ls(new_nodes, transforms=False)
    if not loaded:
        cmds.file(unloadReference='%sRN' % namespace)
    return new_nodes


def change_reference(ref_node, scene_path, loaded=True):
    print('Change reference of %s:%s' % (ref_node, scene_path))

    if not cmds.objExists(ref_node) and not ref_node.endswith("RN"):
        ref_node = cmds.ls(ref_node + "RN")[0]
        if not ref_node:
            print("Cannot find reference %s" % ref_node)
            return

    if not loaded:
        cmds.file(unloadReference=ref_node)

    current_path = os.path.abspath(cmds.referenceQuery(ref_node, filename=True, unresolvedName=True)).rsplit('{', 1)[0]
    print("%s %s" % (current_path, os.path.abspath(scene_path)))
    if current_path == os.path.abspath(scene_path):
        print("\tReference is already from %s" % scene_path)
        return

    file_type = "mayaAscii"
    if scene_path.endswith('.abc'):
        file_type = "Alembic"
    elif scene_path.endswith('.mb'):
        file_type = "mayaBinary"

    new_nodes = cmds.file(
        str(scene_path),
        type=file_type,
        loadReference=str(ref_node),
        returnNewNodes=True,
    )
    return new_nodes


def unload_reference(ref):

    print('Unloading Reference %s' % ref)
    if os.path.dirname(ref):  # is path
        node = cmds.referenceQuery(ref, referenceNode=True)
        ref_scene_path = ref
    else:  # is namespace
        node = ref
        if not node.endswith('RN'):
            node += 'RN'
        try:
            ref_scene_path = cmds.referenceQuery(node, filename=True)
        except Exception as e:
            print("ERROR when unloading %r: %r" % (node, e))
            return
    cmds.file(ref_scene_path, unloadReference=node)


def remove_reference(ref):

    if os.path.isdir(os.path.dirname(ref)):  # is path
        file = ref
        node = cmds.referenceQuery(ref, referenceNode=True)
    else:
        file = cmds.referenceQuery(ref, filename=True)
        node = ref
    print('Remove Reference %s <%s>' % (node, file))

    try:
        cmds.lockNode(node, lock=False)
        cmds.file(file, rfn=node, removeReference=True)
    except Exception as e:
        print("\tCannot remove it: %s" % e)


def reap_references():
    print('Reaping scene references')

    ref_paths = cmds.file(query=True, reference=True)
    visited = set()
    while ref_paths:
        for path in ref_paths:
            visited.add(path)
            try:
                node = cmds.referenceQuery(path, referenceNode=True)
            except RuntimeError as e:
                print(str(e))
            else:
                if cmds.referenceQuery(node, isLoaded=True):
                    namespace = cmds.referenceQuery(path, namespace=True)
                    print("Import file %s" % path)
                    cmds.file(path, importReference=True)
                    if namespace:
                        namespace = namespace[1:]
                        cmds.namespace(mergeNamespaceWithRoot=True, removeNamespace=namespace)
                else:
                    remove_reference(node)

        paths = cmds.file(query=True, reference=True) or list()
        ref_paths = [path for path in paths if path not in visited]


def list_objects_in_namespace(namespace):
    try:
        return cmds.ls(
            cmds.namespaceInfo(namespace, lod=True, r=True, dp=True),
            dagObjects=True
        )
    except RuntimeError:
        print("WARNING: Cannot find %r namespace" % namespace)
    return []


def reference_path(ref, resolved=False):
    node = ref
    if not node.endswith('RN'):
        node += 'RN'
    return cmds.referenceQuery(node, filename=True, unresolvedName=not resolved)


def roots_of(objects, reference=True):
    objects = cmds.ls(objects, transforms=True)
    roots = []
    for node in objects:
        parent = cmds.listRelatives(node, p=True, fullPath=True)
        if not parent or (reference and not cmds.referenceQuery(parent, isNodeReferenced=True)):
            roots.append(node)
    return roots


def reference_roots(ref):
    ref_node = ref
    if not ref_node.endswith('RN'):
        ref_node += 'RN'

    try:
        return roots_of(cmds.referenceQuery(ref_node, nodes=True, dagPath=True))
    except RuntimeError:
        return []


def import_file(scene_path, namespace='', preserve_references=True, parent=None):
    from smks_studio.utils.file import compute_smart_path
    print = print or print_message

    file_type = "mayaAscii"
    if scene_path.endswith('.abc'):
        ensure_alembic_loaded()
        file_type = "Alembic"
    elif scene_path.endswith('.fbx'):
        cmds.loadPlugin('fbxmaya.mll')
        file_type = "FBX"
    elif scene_path.endswith('.mb'):
        file_type = "mayaBinary"

    print('Importing File (%s:)%s as %s' % (namespace, scene_path, file_type))
    if not os.path.isfile(compute_smart_path(scene_path)):
        raise RuntimeError("%s (%s) is not a valid file to import" % (scene_path, file_type))

    if namespace:
        new_nodes = cmds.file(
            scene_path,
            i=True,
            type=file_type,
            namespace=namespace,
            returnNewNodes=True,
            preserveReferences=preserve_references
        )
    else:
        new_nodes = cmds.file(
            scene_path,
            i=True,
            type=file_type,
            returnNewNodes=True,
            preserveReferences=preserve_references
        )
    if parent and cmds.objExists(parent):
        parented = []
        for root in roots_of(new_nodes, False):
            new_root = cmds.parent(root, parent)
            parented += new_root + (cmds.listRelatives(new_root, allDescendents=True, fullPath=True) or [])
        return parented + ls(new_nodes, transforms=False, l=True)
    return new_nodes
