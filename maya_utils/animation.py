#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""List of alembic functions for maya"""

__author__ = "Florentin LUCE"
__email__ = "florentinluce@orange.fr"
__version__ = "1.0.1"


from maya import cmds


def copy_paste_animation(source, target):
    attrList = cmds.listAttr(source, keyable=True)

    shin = cmds.playbackOptions(animationStartTime=True, query=True)
    shout = cmds.playbackOptions(animationEndTime=True, query=True)

    print('source : ', source)
    print('target : ', target)

    if attrList:
        for attr in attrList:
            try:
                cmds.copyKey(source, time=(shin, shout))
                cmds.pasteKey(target, time=(shin, shout), option='replace')
            except:
                continue
