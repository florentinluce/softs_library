#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""List of alembic functions for maya"""

__author__ = "Florentin LUCE"
__email__ = "florentinluce@orange.fr"
__version__ = "1.0.1"


import os
import tempfile

from maya import cmds
from . import * as utils


def ensure_alembic_loaded():
    """Load alembic plugin"""

    if (
            not cmds.pluginInfo('AbcExport', loaded=True, q=True)
            or
            not cmds.pluginInfo('AbcImport', loaded=True, q=True)
    ):
        cmds.loadPlugin('AbcExport')
        cmds.loadPlugin('AbcImport')
    else:
        return



def export_alembic(alembic_path, root=None, objects_list=[], start=0, end=0, scale=1.0):
    """Export an alembic

    Args:
        alembic_path (str): alembic output filepath
        root (str, optional): object from with alembic meshes is exported
        objects_list (list, optional): all objects to export
        start (int, optional): first frame
        end (int, optional): last frame
        scale (float, optional): scale to apply before to export
    """

    ensure_alembic_loaded()

    if not os.path.exists(os.path.dirname(alembic_path)):
        os.makedirs(os.path.dirname(alembic_path))

    if not start or not end:
        start, end = utils.get_range()

    if int(scale) != 1 and root:
        cmds.select(root, hi=True)
        utils.clean_transforms(cmds.ls(sl=True))

        cmds.setAttr("%s.scaleX" % root, scale)
        cmds.setAttr("%s.scaleY" % root, scale)
        cmds.setAttr("%s.scaleZ" % root, scale)

        utils.clean_transforms(cmds.ls(sl=True))

    if int(scale) != 1 and objects_list:

        all = cmds.ls("|*|")
        locator = cmds.spaceLocator(name='ROOT', absolute=True, position=[0, 0, 0])[0]

        for ob in all:
            if ob not in objects_list:
                cmds.parent(ob, locator)

        cmds.setAttr("%s.scaleX" % locator, scale)
        cmds.setAttr("%s.scaleY" % locator, scale)
        cmds.setAttr("%s.scaleZ" % locator, scale)

    cmd = "-file %s -frameRange %s %s -uvWrite -worldSpace -selection" % (alembic_path, start, end)

    if root:
        cmd += " -root %s" % root

    if objects_list:
        cmds.select(objects_list)
        cmd += " -selection"

    cmds.AbcExport(j=cmd)


def export_transforms(parents, alembic_path, scale=1.0):
    """Export an alembic with just transforms animation

    Args:
        parents (list): parents list of the transforms you want to export
        alembic_path (str): alembic output filepath
        scale (float, optional): a scale value to apply before export
    """

    locators = []
    for parent in parents:

        namespace = parent.split(":")[0]

        if not cmds.objExists(parent):
            print("%s does not exists, it won't be exported" % parent)
            continue

        locator = cmds.spaceLocator(name=namespace + ':locator')[0]
        locators.append(locator)
        cmds.parentConstraint(parent, locator)
        cmds.scaleConstraint(parent, locator)

    export_alembic(alembic_path, objects_list=locators, scale=scale)
    cmds.delete(locators)


def export_alembic_camera(camera, alembic_path, start=0, end=0, scale=1.0):
    """Export an alembic with just transforms animation

    Args:
        camera (str): camera shape or transform you want to export
        alembic_path ([type]): alembic output filepath
        start (int, optional): first frame
        end (int, optional): last frame
        scale (float, optional): a scale value to apply before export
    """

    if not start or not end:
        start, end = utils.get_range()

    if cmds.objectType(camera) == "camera":
        cam_shape = camera
        camera = cmds.listRelatives(camera, parent=True)[0]
    else:
        cam_shape = cmds.listRelatives(camera, children=True)[0]

    parent = cmds.listRelatives(camera, parent=True)
    if parent:
        parent = parent[0]

    utils.unlock_transforms(camera)
    cmds.parentConstraint(parent, camera)
    cmds.scaleConstraint(parent, camera)
    cmds.parent(camera, world=True)

    export_alembic(alembic_path, objects_list=[cam_shape], start=start, end=end, scale=scale)


def double_alembic_export(export_set, alembic_path, start=None, end=None, scale=1.0):
    """Export an alembic and apply all hierarchy transforms

    Args:
        export_set (str): the set with objects to export
        alembic_path (str): alembic output filepath
        start ([type], optional): first frame
        end ([type], optional): last frame
        scale (float, optional): scale to apply before to export
    """

    ensure_alembic_loaded()

    if not start or not end:
        start, end = get_range()

    obj_list = []
    for mesh in cmds.sets(export_set, q=True):
        shape = next(child for child in cmds.listRelatives(mesh, shapes=True, f=True) if child.endswith("Shape"))
        obj_list.append(shape)

    # export temp abc
    temp_filepath = os.path.join(tempfile.gettempdir(), "RR", "abc", "from_maya_%s.abc" % export_set)
    export_alembic(temp_filepath, objects_list=obj_list, start=start, end=end)

    # import temp abc and rescale it
    root_loc = cmds.spaceLocator(name='ROOT', absolute=True, position=[0, 0, 0])[0]
    cmds.AbcImport(temp_filepath, m="import", reparent=root_loc)
    export_alembic(alembic_path, root=root_loc, start=start, end=end, scale=scale)

    cmds.select(root_loc, hi=True)
    cmds.delete()
